#include <iostream>
#include <string>
#include "Visit.h"
using std::cout;
using std::endl;
using std::string;

void Visit::setClient(Client client){
	this->client=client;
	data.clientdata=client.getData();
}

void Visit::setReason(string reason){
	this->reason=reason;
	strcpy(data.reason,reason.c_str());
}
void Visit::setData(VISITDATA data){
	date_time=data.date_time;
	interventionNum=data.intervention_num;
	reason=data.reason;
	location=data.location;
	client.setData(data.clientdata);
	this->data=data;
}
void Visit::setInterventionNum(int num){
	this->interventionNum=num;
	data.intervention_num=num;
}
void Visit::setLocation(string location){
	this->location=location;
	strcpy(data.location,location.c_str());
}
string Visit::getReason(){
	return reason;
}
string Visit::getLocation(){
	return location;
}
void Visit::display(){
	cout<<"\n\n\t\t\t\tVISIT DETAILS:"<<endl;
	cout<<"\t\t\t\t\t\t\tDATE: "<<date_time;
	cout<<"\n\t\t\t\t\t\t\tINTERVENTION NUMBER: "<<interventionNum<<endl;
	cout<<"\t\t\t\t\t\t\tREASON: "<<reason<<endl;
	cout<<"\t\t\t\t\t\t\tLOCATION: "<<location<<endl;
	client.display();
}