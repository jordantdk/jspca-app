#ifndef MENU_H
#define MENU_H
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include "misc.h"

using std::cout;
using std::string;
using std::vector;

//void printLogo(string filename);
//int getch(void);

class Menu{
private:
	vector <string> options;
	int size;
	string instructions;
public:
	Menu(string options[],int size,string instructions);
	Menu(int size,string instructions);
	Menu(string options[],int size);
	void setOptions(string options[]);
	void setInstructions(string text);
	void arrow(int count);
	int prompt();
};

#endif