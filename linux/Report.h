#ifndef REPORT_H
#define REPORT_H
#include "Database.h"
#include <string>
#include <iostream>
#include <cstring>
#include <ctime>
using std::string;

const string types[2]={"VISIT","REMOVAL"};

class Report{
private:
	string date_time;
	string type;
public:
	Report(){
		type=types[0];
		time_t now= time(0);
		date_time=ctime(&now);
		date_time.pop_back();
	}
	Report(int type){
		this->type=types[type];
		time_t now= time(0);
		date_time=ctime(&now);
		date_time.pop_back();
	}
	void setType(int type){
		this->type=types[type];
	}
	void printHeader();//prints logo
	void locationReport();
	void dateReport();
	void AnimalReport();
	void outcomeReport();
	void getLocation(string& text);
	string getDate();
	string getAnimal();
	////////////////////////////////////////////
	void displayRecords(string location,string date,string animalType,string outcome);//done
	void displaySingleVisit(VISITDATA data);//done
	void displaySingleRemoval(REMOVALDATA data);//done
};
#endif
