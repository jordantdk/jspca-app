#ifndef DATABASE_H
#define DATABASE_H
#include <iostream>
#include "Visit.h"
#include "Removal.h"
#include "Menu.h"
#include <string>
#include <fstream>

using std::fstream;
using std::string;

class Database{
private:
	fstream file;
public:

	void viewAll(string objectName);//displays all objects of type <objectname>
	int getNextID(string filename,int obj_size);//generates id based on no. of records in file
	void addVisit(Visit object);
	void addRemoval(Removal object);
	void viewVisit();
	void viewRemoval();
	void view(int indicator);
	void viewClient(int type);
	void viewCByID(int id,int type);
	void viewCByName(string name,int type);
	void viewCByContact(string contact,int type);
	void viewVByID(int id);
	void viewVbyName(string name);
	void viewRByID(int id);
	void viewRByName(string name);
	void getInput(int temp,int &id,string &name);
	void update(int indicator,bool toDelete=false);
	//////////////////////////////////
	/*
	boolean parameter toDelete tells the functions whether or not the record that is searched 
	for is to be deleted otherwise false for update
	*/
	void updateAnimal(ANIMALDATA *adata);
	void updateClient(int type,bool toDelete);//prompts for search key
	void updateC(int id,string name,string contact,int type,bool toDelete);
	void getUpdateDataForC(CLIENTDATA *cdata);//prompts user for updated data
	void getAbilityToMakePayment(CLIENTDATA *cdata);
	void updateVisit(bool toDelete);//prompts for search key
	void updateRemoval(bool toDelete);//prompts for search key
	void updateVByID(int id,bool toDelete);
	void updateVByName(string name,bool toDelete);
	void updateRByID(int id,bool toDelete);
	void updateRByName(string name,bool toDelete);
	void getUpdateDataForRemoval(REMOVALDATA *data);//prompts user for updated data
	void getUpdateDataForVisit(VISITDATA *data);//prompts user for updated data
	void getUpdatedLocation(VISITDATA *data);//prompts user for updated location
	void positionCursor(fstream *file,int size);//positions the cursor in file when updating
};
#endif
