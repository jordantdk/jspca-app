#include <iostream>
#include "Animal.h"
#include <fstream>
using std::fstream;
using std::ios;
using std::cout;
using std::endl;
using std::string;
using namespace std;

void Animal::setOwnerID(int id){
	this->ownerID=id;
	this->container.ownerID=id;
}
int Animal::getOwnerID(){
	return this->ownerID;
}
void Animal::setKey(int key){
	this->key=key;
	this->container.key=key;
}
int Animal::getKey(){
	return key;
}
void Animal::setType(string type){
	this->type=type;
	strcpy(container.type,type.c_str());
}
void Animal::setBreed(string breed){
	this->breed=breed;
	strcpy(container.breed,breed.c_str());
}
void Animal::setGender(string gender){
	this->gender=gender;
	strcpy(container.gender,gender.c_str());
}
void Animal::setAge(int age){
	this->age= age;
	container.age=age;
}
void Animal::setName(string name){
	this->name=name;
	strcpy(container.name,name.c_str());
}
string Animal::getName(){
	return name;
}
string Animal::getType(){
	return type;
}
string Animal::getBreed(){
	return breed;
}
string Animal::getGender(){
	return gender;
}
int Animal::getAge(){
	return age;
}
void Animal::display(){
	cout<<"\t\t\t\t\t\t\tNAME: "<<name<<endl;
	cout<<"\t\t\t\t\t\t\tTYPE: "<<type<<endl;
	cout<<"\t\t\t\t\t\t\tBREED: "<<breed<<endl;
	cout<<"\t\t\t\t\t\t\tGENDER: "<<gender<<endl;
	cout<<"\t\t\t\t\t\t\tAGE: "<<age<<endl;
}