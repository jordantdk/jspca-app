#ifndef REQUEST_H
#define REQUEST_H
#include <iostream>
#include <string>
#include <ctime>
#include <cstring>
#include "Client.h"
using std::string;
using std::cout;
using std::endl;

class Request{
protected:
	Client client;
	string date_time;
	int interventionNum;
public:
	Request(){
		time_t now= time(0);
		date_time=ctime(&now);
		interventionNum=0;
		date_time.pop_back();
	}
	Request(Client client,int interventionNum,string type){
		this->interventionNum=interventionNum;
		time_t now= time(0);
		date_time=ctime(&now);
		date_time.pop_back();
		this->client=client;
	}
	Request(int interventionNum){
		this->interventionNum=interventionNum;
	}
	void setClient(Client client);
	void setInterventionNum(int num);
	Client getClient();
	string getDate();
	int getInterventionNum();
	void display();
	~Request(){}
};

#endif
