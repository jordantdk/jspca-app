#ifndef VISIT_H
#define VISIT_H
#include <iostream>
#include <string>
#include <cstring>
#include "Request.h"
using std::cout;
using std::endl;
using std::string;

/*
subclass for writing visit data to file
*/
typedef class vi_data{
public:
	int intervention_num;
	char date_time[30];
	char reason[40];
	char location[20];
	CLIENTDATA clientdata;
	vi_data(){}
}VISITDATA;

class Visit: public Request{
private:
	string reason;
	string location;
	VISITDATA data;
public:
	Visit(){
		reason="";
		location="";
		updateData();
	}
	Visit(Client client,string reason,string location){
		this->client=client;
		this->reason=reason;
		this->location=location;
		updateData();
	}
	Visit(VISITDATA  data){
		date_time=data.date_time;
		interventionNum=data.intervention_num;
		reason=data.reason;
		location=data.location;
		client.setData(data.clientdata);
		this->data=data;
	}
	void updateData(){
		data.intervention_num=interventionNum;
		strcpy(data.date_time,date_time.c_str());
		strcpy(data.reason,reason.c_str());
		strcpy(data.location,location.c_str());
		data.clientdata=client.getData();
	}
	VISITDATA getData(){
		return data;
	}
	void setClient(Client client);
	void setData(VISITDATA data);
	void setInterventionNum(int num);
	void setReason(string reason);
	void setLocation(string location);
	string getReason();
	string getLocation();
	void display();
	~Visit(){}
};


#endif
