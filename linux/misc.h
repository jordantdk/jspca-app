#ifndef MISC_H
#define MISC_H
#include <cstdio>
#include <cstdlib>
#include <termios.h>
#include <unistd.h>
#include <iostream>
#include <fstream>

#define UP 65
#define DOWN 66
#define LEFT 68
#define RIGHT 67

using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::ifstream;

void printLogo(string filename);
void wait();
int getch(void);

#endif