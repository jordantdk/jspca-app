#include <vector>
#include <iostream>
#include <fstream>
#include <cstdlib>
#if (defined (_WIN32) || defined (_WIN64))
	#include <conio.h>//file that contains getch on windows
#elif (defined (LINUX) || defined (__linux__))
	#include <termios.h>
	#include <unistd.h>
#endif
#include <string>
#include <ctime>
#include "Menu.h"

using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::vector;
using std::ifstream;

Menu::Menu(string options[],int size,string instructions){
	this->size=size;
	this->instructions=instructions;
	for(int i=0;i<size;i++){
		this->options.push_back(options[i]);
	}
}
Menu::Menu(int size,string instructions){
	this->size=size;
	this->instructions=instructions;
}
Menu::Menu(string options[],int size){
	this->size=size;
	for(int i=0;i<size;i++){
		this->options.push_back(options[i]);
	}
}
//populates string vector options with string array passed
void Menu::setOptions(string options[]){
	for(int i=0;i<size;i++){
		this->options.push_back(options[i]);
	}
}
void Menu::setInstructions(string text){
	instructions=text;
}
//prints out logo, options and arrow pointing on option with array index count
void Menu::arrow(int count){
	int i;
	string date_time;
	time_t now= time(0);
	date_time=ctime(&now);
	date_time.pop_back();//gets rid of newline character
	printLogo("logo.txt");
	cout<<"\n\n\t\t\t\t\t\t\t "<<date_time;
	cout<<instructions;
	for(i=0;i<size;i++){
		cout<<"\t\t\t\t\t\t    "<<((i==count)?"<<<-->>>":"        ")<<options[i]<<((i==count)?"<<<-->>>":"        ")<<"\n"<<endl;
	}
}
//returns the array index indicating which option was selected
int Menu::prompt(){
	int key;
	unsigned int count=0;
	arrow(count);
	while(key!='\n'){
		key=getch();
		if(key==DOWN){
			if(count<(size-1)){
				count++;
			}
		}else if(key==UP && count>0){
				count--;
		}
		arrow(count);//prints out logo, options and arrow pointing on option with array index count
	}
	return count;
}
