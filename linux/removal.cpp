#include <iostream>
#include <string>
#include "Removal.h"
using std::cout;
using std::endl;
using std::string;

void Removal::setClient(Client client){
	this->client=client;
	data.clientdata=client.getData();
}
void Removal::setData(REMOVALDATA data){
	interventionNum=data.intervention_num;
	date_time=data.date_time;
	address=data.address;
	isAdopted=data.isAdopted;
	outcome=data.outcome;
	client.setData(data.clientdata);
	//updateData();
	this->data=data;
}
void Removal::setInterventionNum(int num){
	this->interventionNum=num;
	data.intervention_num=num;
}
void Removal::setAddress(string address){
	this->address=address;
	strcpy(data.address,address.c_str());
}
void Removal::setOutcome(string outcome){
	this->outcome=outcome;
	strcpy(data.outcome,outcome.c_str());
}
void Removal::adoption(bool value){
	isAdopted=value;
}
string Removal::getAddress(){
	return address;
}
string Removal::getOutcome(){
	return outcome;
}
void Removal::display(){
	cout<<"\n\n\t\t\t\tREMOVAL DETAILS:\n"<<endl;
	cout<<"\t\t\t\t\t\t\tDATE: "<<date_time;
	cout<<"\n\t\t\t\t\t\t\tINTERVENTION NUMBER: "<<interventionNum<<endl;
	cout<<"\t\t\t\t\t\t\tADDRESS: "<<address<<endl;
	cout<<"\t\t\t\t\t\t\tOUTCOME: "<<outcome<<endl;
	cout<<"\t\t\t\t\t\t\tADOPTED: "<<((isAdopted)? "YES":"NO")<<endl;
	client.display();
}