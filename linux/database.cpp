#include "Database.h"
#include "Visit.h"
#include "Removal.h"
#include <vector>
#include <string>
#include <cstring>
#include <fstream>
#include <iostream>
using namespace std;

void Database::viewAll(string objectName){
	ifstream file;
	VISITDATA vdata;
	REMOVALDATA rdata;
	Removal robj;
	Visit vobj;
	printLogo("logo.txt");
	if(objectName=="visit"){
		file.open("visits.dat",ios::in|ios::binary);
		file.seekg(0,file.beg);
		if(!file){
			printLogo("error.txt");
			cout<<"\n\n\n\t\t\t\t\tError opening file. It seems like there are NO visits yet."<<endl;
		}else{
			while(file.read((char*)&vdata,sizeof(vdata))){
				if(vdata.intervention_num==0){
					continue;//if it is marked as deleted
				}
				vobj.setData(vdata);
				vobj.display();
				cout<<"\n\n";
			}
		}
	}else if(objectName=="removal"){
		file.open("removals.dat",ios::in|ios::binary);
		file.seekg(0,file.beg);
		if(!file){
			printLogo("error.txt");
			cout<<"\n\n\n\t\t\t\t\tError opening file. It seems like there are NO removals yet."<<endl;
		}else{
			while(file.read((char*)&rdata,sizeof(rdata))){
				if(rdata.intervention_num==0){
					continue;//if it is marked as deleted
				}
				robj.setData(rdata);
				robj.display();
			}
		}
	}
	wait();
}
void Database::addVisit(Visit object){
	int objData_size=sizeof(object.getData());
	object.setInterventionNum(getNextID("visits.dat",objData_size));
	Client client=object.getClient();
	client.setKey(object.getInterventionNum());
	object.setClient(client);
	VISITDATA data = object.getData();
	file.open("visits.dat",ios::app|ios::binary);
	file.write((char*)&data,sizeof(data));
	file.close();
	printLogo("success.txt");
	cout<<"\n\n\n\t\t\t\t\t\tENTRY WAS ADDED SUCCESSFULLY.\n";
	wait();
}
void Database::addRemoval(Removal object){
	int objData_size=sizeof(object.getData());
	object.setInterventionNum(getNextID("removals.dat",objData_size));
	Client client=object.getClient();
	client.setKey(object.getInterventionNum());
	object.setClient(client);
	REMOVALDATA data = object.getData();
	file.open("removals.dat",ios::app|ios::binary);
	file.write((char*)&data,sizeof(data));
	file.close();
	printLogo("success.txt");
	cout<<"\n\n\n\t\t\t\t\t\tENTRY WAS ADDED SUCCESSFULLY.\n";
	wait();
}
int Database::getNextID(string filename,int obj_size){
	ifstream file;
	int len;
	int newID;
	file.open(filename.c_str(),ios::in|ios::binary);
	file.seekg(0,file.end);//positions cursor to end of file
	len=(file.tellg()/obj_size)+1;// gets new position (# of objects + 1)
	newID=len*10;//id increments by 10
	return newID;
}
void Database::view(int indicator){//indicator indicates object type
	Client client;
	Visit visit;
	Removal removal;
	string options[3]={"[  CLIENT  ]","[  VISIT   ]","[ GO  BACK ]"};
	if(indicator==1){
		options[1]="[  REMOVAL ]";
	}
	string instructions="\n\n\t\t\t\t\t\t\t       VIEW MENU\n\n\t\t\t\t\t\tSELECT THE ENTITY YOU WOULD LIKE TO VIEW\n\n\n";
	Menu menu(options,3,instructions);
	int temp=9;
	while(temp!=2){
		temp=menu.prompt();
		switch(temp){
			case 0:
				viewClient(indicator);
			break;
			case 1:
				if(indicator==0){
					viewVisit();
				}else{
					viewRemoval();	
				}
		}
	}
}
void Database::viewClient(int type){//type indicates object type
	int id;
	string name;
	string contactNumber;
	Client client;
	string options[4]={"[   SYSTEM  ID   ]","[      NAME      ]","[ CONTACT NUMBER ]","[    GO BACK     ]"};
	string instructions="\n\n\t\t\t\t\t\t\t      VIEW CLIENT\n\n\t\t\t\t\t\tSELECT THE TYPE OF INFORMATION YOU HAVE FOR THE CLIENT\n\n";
	Menu menu(options,4,instructions);
	int temp=menu.prompt();
	printLogo("logo.txt");
	switch(temp){
		case 0:cout<<"\n\n\n\t\t\t\t\t\tENTER THE SYSTEM ID OF THE CUSTOMER: ";
			cin>>id;
			viewCByID(id,type);
		break;
		case 1:cout<<"\n\n\n\t\t\t\t\t\tENTER THE FULL NAME OF THE CUSTOMER: ";
			getline(cin,name);
			viewCByName(name,type);
		break;
		case 2:cout<<"\n\n\n\t\t\t\t\t\tENTER THE PHONE NUMBER OF THE CUSTOMER: ";
			cin>>contactNumber;
			viewCByContact(contactNumber,type);
	}
}
void Database::viewCByID(int id,int type){
	ifstream Vfile;//file object to access visit records 
	ifstream Rfile;//file object to access removal records
	bool found=false;
	Client client;
	VISITDATA vdata;
	Visit visit;
	REMOVALDATA rdata;
	Removal removal;
	Vfile.open("visits.dat",ios::in|ios::binary);
	if(Vfile && type==0){
		while(Vfile.read((char*)&vdata,sizeof(vdata))){
			if(vdata.intervention_num==0){
					continue;//if it is marked as deleted
			}
			visit.setData(vdata);
			client=visit.getClient();
			if(client.getKey()==id){
				found=true;
				client.display();
				break;
			}
		}
	}
	if(!found){
		Rfile.open("removals.dat",ios::in|ios::binary);
		if(Rfile && type==1){
			while(Rfile.read((char*)&rdata,sizeof(rdata))){
				if(rdata.intervention_num==0){
					continue;//if it is marked as deleted
				}
				removal.setData(rdata);
				client=removal.getClient();
				if(client.getKey()==id){
					found=true;
					client.display();
					break;
				}
			}
		}
	}
	if(!found){
		printLogo("error.txt");
		cout<<"\n\n\n\t\t\t\t\t\tERROR: NO CLIENT WAS FOUND WITH THE GIVEN ID.\n";
	}
	getch();
	wait();
}
void Database::viewCByName(string name,int type){
	ifstream Vfile;//file object to access visit records 
	ifstream Rfile;//file object to access removal records
	bool found=false;
	Client client;
	VISITDATA vdata;
	Visit visit;
	REMOVALDATA rdata;
	Removal removal;
	Vfile.open("visits.dat",ios::in|ios::binary);
	if(Vfile && type==0){
		while(Vfile.read((char*)&vdata,sizeof(vdata))){
			if(vdata.intervention_num==0){
					continue;//if it is marked as deleted
			}
			visit.setData(vdata);
			client=visit.getClient();
			if(client.getName()==name){
				found=true;
				client.display();
				break;
			}
		}
	}
	if(!found){
		Rfile.open("removals.dat",ios::in|ios::binary);
		if(Rfile && type==1){
			while(Rfile.read((char*)&rdata,sizeof(rdata))){
				if(rdata.intervention_num==0){
					continue;//if it is marked as deleted
				}
				removal.setData(rdata);
				client=removal.getClient();
				if(client.getName()==name){
					found=true;
					client.display();
					break;
				}
			}
		}
	}
	if(!found){
		printLogo("error.txt");
		cout<<"\n\n\n\t\t\t\t\t\tERROR: NO CLIENT WAS FOUND WITH THE GIVEN NAME."<<endl;
	}
	getch();
	wait();
}
void Database::viewCByContact(string contact,int type){
	contact="(876)"+contact;
	ifstream Vfile;//file object to access visit records 
	ifstream Rfile;//file object to access removal records
	bool found=false;
	Client client;
	VISITDATA vdata;
	Visit visit;
	REMOVALDATA rdata;
	Removal removal;
	Vfile.open("visits.dat",ios::in|ios::binary);
	if(Vfile && type==0){
		while(Vfile.read((char*)&vdata,sizeof(vdata))){
			if(vdata.intervention_num==0){
					continue;//if it is marked as deleted
			}
			visit.setData(vdata);
			client=visit.getClient();
			if(client.getContact()==contact){
				found=true;
				client.display();
				break;
			}
		}
	}
	if(!found){
	Rfile.open("removals.dat",ios::in|ios::binary);
		if(Rfile && type==1){
			while(Rfile.read((char*)&rdata,sizeof(rdata))){
				if(rdata.intervention_num==0){
					continue;//if it is marked as deleted
				}
				removal.setData(rdata);
				client=removal.getClient();
				if(client.getContact()==contact){
					found=true;
					client.display();
					break;
				}
			}
		}
	}
	if(!found){
		printLogo("error.txt");
		cout<<"\n\n\n\t\t\t\t\t\tERROR: NO CLIENT WAS FOUND WITH THE GIVEN CONTACT."<<endl;
	}
	getch();
	wait();
}
//prompts for search key 
void Database::viewVisit(){
	int temp;
	int id;
	string name;
	string options[3]={"[ INTERVENTION # ]","[  CLIENT  NAME  ]","[    GO  BACK    ]"};
	string instructions="\n\n\t\t\t\t\t\t\t     SEARCH VISIT\n\n\t\t\t\t\t     SELECT THE TYPE OF INFORMATION YOU WISH TO PROVIDE\n\n";
	Menu menu(options,3,instructions);
	temp=menu.prompt();
	printLogo("logo.txt");
	if(temp!=2){
		getInput(temp,id,name);
		switch(temp){
			case 0:
				viewVByID(id);
			break;
			case 1:
				viewVbyName(name);
		}
	}

}
void Database::viewVByID(int id){
	ifstream file;
	Visit visit;
	bool found=false;
	VISITDATA data;
	file.open("visits.dat",ios::in|ios::binary);
	if(file){
		while(file.read((char*)&data,sizeof(data))){
			if(data.intervention_num==0){
					continue;//if it is marked as deleted
			}
			if(data.intervention_num==id){
				found=true;
				visit.setData(data);
				visit.display();
				break;
			}
		}
	}
	if(!found){
		printLogo("error.txt");
		cout<<"\n\n\n\t\t\t\t\t\tERROR: NO VISIT WAS FOUND WITH THE GIVEN INTERVENTION NUMBER."<<endl;
	}
	getch();
	wait();
}
void Database::viewVbyName(string name){
	ifstream file;
	Visit visit;
	bool found=false;
	Client client;
	VISITDATA data;
	file.open("visits.dat",ios::in|ios::binary);
	if(file){
		while(file.read((char*)&data,sizeof(data))){
			if(data.intervention_num==0){
					continue;//if it is marked as deleted
			}
			visit.setData(data);
			client=visit.getClient();
			if(client.getName()==name){
				found=true;
				visit.display();
				break;
			}
		}
	}
	if(!found){
		printLogo("error.txt");
		cout<<"\n\n\n\t\t\t\t\t\tERROR: NO VISIT WAS FOUND WITH THE GIVEN CLIENT NAME."<<endl;
	}
	getch();
	wait();
}
//prompts for search key 
void Database::viewRemoval(){
	int temp;
	int id;
	string name;
	string options[3]={"[ INTERVENTION # ]","[  CLIENT  NAME  ]","[    GO  BACK    ]"};
	string instructions="\n\n\t\t\t\t\t\t\t     SEARCH REMOVAL\n\n\t\t\t\t\t\tSELECT THE TYPE OF INFORMATION YOU WISH TO PROVIDE\n\n";
	Menu menu(options,3,instructions);
	temp=menu.prompt();
	printLogo("logo.txt");
	if(temp!=2){
		getInput(temp,id,name);
		if(temp==0){
			viewRByID(id);
		}else{
			viewRByName(name);
		}
	}

}
void Database::viewRByID(int id){
	ifstream file;
	bool found=false;
	REMOVALDATA data;
	Removal removal;
	file.open("removals.dat",ios::in|ios::binary);
	if(file){
		while(file.read((char*)&data,sizeof(data))){
			if(data.intervention_num==0){
					continue;//if it is marked as deleted
			}
			if(data.intervention_num==id){
				removal.setData(data);
				found=true;
				removal.display();
				break;
			}
		}
	}
	if(!found){
		printLogo("error.txt");
		cout<<"\n\n\n\t\t\t\t\tERROR: NO REMOVAL WAS FOUND WITH THE GIVEN INTERVENTION NUMBER.\n";
	}
	getch();
	wait();
}
void Database::viewRByName(string name){
	ifstream file;
	bool found=false;
	REMOVALDATA data;
	Client client;
	Removal removal;
	file.open("removals.dat",ios::in|ios::binary);
	if(file){
		while(file.read((char*)&data,sizeof(data))){
			if(data.intervention_num==0){
					continue;//if it is marked as deleted
			}
			removal.setData(data);
			client=removal.getClient();
			if(client.getName()==name){
				found=true;
				removal.display();
				break;
			}
		}
	}
	if(!found){
		printLogo("error.txt");
		cout<<"\n\n\n\t\t\t\t\t\tERROR: NO REMOVAL WITH THE GIVEN CLIENT NAME.\n";
	}
	getch();
	wait();
}
/*function that prompts user for the key to search by based on an integer (type) and 
assigns value by reference
*/
void Database::getInput(int temp,int &id,string &name){
	switch(temp){
		case 0: cout<<"\n\n\n\t\t\t\t\t\tENTER THE INTERVENTION NUMBER: ";
		cin>>id;
		break;
		case 1: cout<<"\n\n\n\t\t\t\t\t\tENTER THE FULL NAME OF THE CLIENT: ";
		getline(cin,name);
		break;
	}
}
/////////////////////////////////////////////////////////////////////////////
void Database::update(int indicator,bool toDelete){
	string options[3]={"[   CLIENT   ]","[]","[  GO  BACK  ]"};
	string action;
	if(toDelete){
		action="DELETE";
	}else{
		action="UPDATE";
	}
	string instructions="\n\n\t\t\t\t\t\t\t     "+action+" MENU\n\n\t\t\t\t\t\tSELECT THE TYPE OF RECORD YOU WANT TO "+action+"\n\n";
	if(indicator==0){
		options[1]="[   VISIT    ]";
	}else{
		options[1]="[   REMOVAL  ]";
	}
	Menu menu(options,3,instructions);
	int temp=menu.prompt();
	switch(temp){
		case 0:
			updateClient(indicator,toDelete);
			break;
		case 1:
			if(indicator==0){
				updateVisit(toDelete);
			}else{
				updateRemoval(toDelete);
			}
	}
}
void Database::updateClient(int type,bool toDelete){
	int id=0;
	string name="";
	string contact="";
	string action;
	string options[4]={"[   SYSTEM  ID   ]","[      NAME      ]","[ CONTACT NUMBER ]","[    GO BACK     ]"};
	if(toDelete){
		action="DELETE";
	}else{
		action="UPDATE";
	}
	string instructions="\n\n\t\t\t\t\t\t\t      "+action+" CLIENT\n\n\t\t\t\t\t\tSELECT THE TYPE OF INFORMATION YOU HAVE FOR THE CLIENT\n\n";
	Menu menu(options,4,instructions);
	int temp=menu.prompt();
	printLogo("logo.txt");
	switch(temp){
		case 0:cout<<"\n\n\n\t\t\t\t\t\tENTER THE SYSTEM ID OF THE CUSTOMER: ";
			cin>>id;
			updateC(id,name,contact,type,toDelete);
		break;
		case 1:cout<<"\n\n\n\t\t\t\t\t\tENTER THE FULL NAME OF THE CUSTOMER: ";
			getline(cin,name);
			updateC(id,name,contact,type,toDelete);
		break;
		case 2:cout<<"\n\n\n\t\t\t\t\t\tENTER THE PHONE NUMBER OF THE CUSTOMER: ";
			cin>>contact;
			updateC(id,name,contact,type,toDelete);
	}

}
void Database::updateC(int id,string name,string contact,int type,bool toDelete){
	fstream vfile;
	fstream rfile;
	bool found=false;
	VISITDATA vdata;
	REMOVALDATA rdata;
	CLIENTDATA cdata;
	Client client;
	Visit visit;
	Removal removal;
	vfile.open("visits.dat",ios::in|ios::binary|ios::out);
	rfile.open("removals.dat",ios::in|ios::binary|ios::out);
	if(vfile && type==0){
		while(vfile.read((char*)&vdata,sizeof(vdata))){
			cdata=vdata.clientdata;
			if(id!=0 && cdata.key==id){
				found=true;
			}else if(name!=""){
				visit.setData(vdata);
				client=visit.getClient();
				if(client.getName()==name){
					found=true;
				}
			}else if(contact!=""){
				visit.setData(vdata);
				client=visit.getClient();
				if(client.getContact()==contact){
					found=true;
				}
			}
			if(toDelete && found){
					vdata.intervention_num=0;///sets id to 0 (id with value 0 is marked as deleted)
			}else if(found){
					getUpdateDataForC(&cdata);
					vdata.clientdata=cdata;
			}
			if(found){
				//goes back 1 record, writes new data over old data
				positionCursor(&vfile,sizeof(vdata));
				vfile.write((char*)&vdata,sizeof(vdata));
				break;
			}
		}
	}
	if(rfile &&	type==1){
		while(rfile.read((char*)&rdata,sizeof(rdata))){
			cdata=rdata.clientdata;
			if(id!=0 && cdata.key==id){
				found=true;
			}else if(name!=""){
				client.setData(cdata);
				if(client.getName()==name){
					found=true;
				}
			}else if(contact!=""){
				client.setData(cdata);
				if(client.getContact()==contact){
					found=true;
				}
			}
			if(toDelete && found){
				rdata.intervention_num=0;//sets id to 0 (id with value 0 is marked as deleted)
			}else if(found){
				getUpdateDataForC(&cdata);
				rdata.clientdata=cdata;
			}
			if(found){
				//goes back 1 record, writes new data over old data
				positionCursor(&rfile,sizeof(rdata));
				rfile.write((char*)&rdata,sizeof(rdata));
				break;
			}
		}
	}
	if(!found){
			printLogo("error.txt");
			cout<<"\n\n\n\t\t\t\t\t\tERROR: THERE ARE NO RECORDS IN THE FILE WITH THAT CLIENT\n";
	}else{
		printLogo("success.txt");
		cout<<"\n\n\n\t\t\t\t\t\tINFORMATION WAS "<<((toDelete)? "DELETED":"UPDATED")<<" SUCCESSFULLY.\n";
		wait();
	}
	wait();
}
void Database::getUpdateDataForC(CLIENTDATA *cdata){
	int indicator=9;
	float deposit=0.0;
	ANIMALDATA adata=cdata->animal_data;
	while(!(indicator==7)){
		printLogo("logo.txt");
		cout<<"\n\n\t\t\t\tPRESS THE CORRESPONDING NUMBER TO UPDATE THE INFORMATION OR  7 TO CONTINUE"<<endl;
		cout<<"\n\n\t\t\t\t\t1. FIRST NAME: "<<cdata->firstName<<endl;
		cout<<"\t\t\t\t\t2. LAST NAME: "<<cdata->lastName<<endl;
		cout<<"\t\t\t\t\t3. CONTACT NUMBER: "<<cdata->contactNumber<<endl;
		cout<<"\t\t\t\t\t4. ABILITY TO MAKE PAYMENT: "<<cdata->abilityToMakePayment<<endl;
		if(strcmp(cdata->abilityToMakePayment,"ABLE")==0 || strcmp(cdata->abilityToMakePayment,"DONATION")==0){
			cout<<"\t\t\t\t\t5. DEPOSIT: "<<cdata->deposit<<endl;
		}
		cout<<"\t\t\t\t\t6. UPDATE ANIMAL"<<endl;
		cout<<"\t\t\t\t\t7. CONTINUE";
		cout<<"\n\n\t\t\t\t\tENTER:";
		indicator=getch();
		indicator-=48;
		switch(indicator){
			case 1:
				cout<<"\n\t\t\t\t\tENTER FIRST NAME: ";
				cin.getline(cdata->firstName,25);
				break;
			case 2:
				cout<<"\n\t\t\t\t\tENTER LAST NAME: ";
				cin.getline(cdata->lastName,25);
				break;
			case 3:
				cout<<"\n\t\t\t\t\tENTER CONTACT NUMBER: ";
				cin.getline(cdata->contactNumber,14);
				break;
			case 4:
				getAbilityToMakePayment(cdata);
				break;
			case 5:
				if(strcmp(cdata->abilityToMakePayment,"ABLE")!=0 && strcmp(cdata->abilityToMakePayment,"DONATION")!=0){
					getAbilityToMakePayment(cdata);
					if(strcmp(cdata->abilityToMakePayment,"UNABLE")==0){
						cdata->deposit=0;
					}
				}
					cout<<"\n\t\t\t\t\tENTER THE CLIENT'S NEW DEPOSIT: ";
					cin>>deposit;
					cdata->deposit+=deposit;
				break;
			case 6:updateAnimal(&adata);
					cdata->animal_data=adata;
			}
		}
	}
void Database::updateAnimal(ANIMALDATA *adata){
	int indicator;
	while(!(indicator==5)){
		printLogo("logo.txt");
		getch();
		cout<<"\n\n\t\t\t\tPRESS THE CORRESPONDING NUMBER TO UPDATE THE INFORMATION OR  5 TO SAVE"<<endl;
		cout<<"\n\t\t\t\t\t1. NAME: "<<adata->name<<endl;
		cout<<"\t\t\t\t\t2. TYPE: "<<adata->type<<endl;
		cout<<"\t\t\t\t\t3. BREED: "<<adata->breed<<endl;
		cout<<"\t\t\t\t\t4. AGE: "<<adata->age<<endl;
		cout<<"\t\t\t\t\t5. SAVE"<<endl;
		cout<<"\n\n\t\t\t\t\tENTER: ";
		indicator=getch();
		indicator-=48;
		if(indicator==4){
			cout<<"\n\n\t\t\t\t\t\tENTER UPDATED AGE: ";
			cin>>adata->age;
		}else if(indicator<=3 && indicator>=1){
			switch(indicator){
				case 1:
					cout<<"\n\n\t\t\t\t\t\tENTER UPDATED NAME: ";
					cin.getline(adata->name,25);
					break;
				case 2:
					cout<<"\n\n\t\t\t\t\t\tENTER UPDATED TYPE: ";
					cin.getline(adata->type,25);
					break;
				case 3:
					cout<<"\n\n\t\t\t\t\t\tENTER UPDATED BREED: ";
					cin.getline(adata->breed,25);
					break;
			}
		}
	}
}
void Database::getAbilityToMakePayment(CLIENTDATA *cdata){
	string options[3]={"[   ABLE   ]","[  UNABLE  ]","[ DONATION ]"};
	string instructions="\n\n\t\t\t\t\t\t   CLIENT'S ABILITY TO MAKE PAYMENT\n\n\n";
	Menu menu(options,3,instructions);
	switch(menu.prompt()){
		case 0:
			strcpy(cdata->abilityToMakePayment,"ABLE");
			break;
		case 1:
			strcpy(cdata->abilityToMakePayment,"UNABLE");
			break;
		case 2:
			strcpy(cdata->abilityToMakePayment,"DONATION");
			break;
	}

}
/*takes a pointer to a file object and the size of the object(visitdata or removaldata)
and moves back cursor 1 * number of bytes(size) from the current position
*/
void Database::positionCursor(fstream *file,int size){
	int len=file->tellp();
	file->seekg(len-size,ios::beg);
}
//////////////////////////////////////////////////////////////////////
/*
void Database::updateVisit();
void Database::updateRemoval();
*/
void Database::updateVisit(bool toDelete){
	int temp;
	int id;
	string name;
	string options[3]={"[ INTERVENTION # ]","[  CLIENT  NAME  ]","[    GO  BACK    ]"};
	string instructions="\n\n\t\t\t\t\t\t\t     SEARCH VISIT\n\n\t\t\t\t\t     SELECT THE TYPE OF INFORMATION YOU WISH TO PROVIDE\n\n";
	Menu menu(options,3,instructions);
	temp=menu.prompt();
	printLogo("logo.txt");
	if(temp!=2){
		getInput(temp,id,name);
		switch(temp){
			case 0:
				updateVByID(id,toDelete);
			break;
			case 1:
				updateVByName(name,toDelete);
		}
	}
}
void Database::updateVByID(int id,bool toDelete){
	fstream file;
	Visit visit;
	bool found=false;
	VISITDATA data;
	file.open("visits.dat",ios::in|ios::binary|ios::out);
	if(file){
		while(file.read((char*)&data,sizeof(data))){
			if(data.intervention_num==id){
				found=true;
				if(toDelete){
					data.intervention_num=0;//sets id to 0 (id with value 0 is marked as deleted)
				}else{
					getUpdateDataForVisit(&data);
				}
				positionCursor(&file,sizeof(data));
				file.write((char*)&data,sizeof(data));
				break;
			}
		}
	}
	if(!found){
		printLogo("error.txt");
		cout<<"\n\n\n\t\t\t\t\t\tERROR: NO VISIT WAS FOUND WITH THE GIVEN INTERVENTION NUMBER."<<endl;
	}else{
		printLogo("success.txt");
		cout<<"\n\n\n\t\t\t\t\t\tINFORMATION WAS "<<((toDelete)? "DELETED":"UPDATED")<<" SUCCESSFULLY.\n";
	}
	getch();
	wait();
}
void Database::updateVByName(string name,bool toDelete){
	fstream file;
	Visit visit;
	bool found=false;
	VISITDATA data;
	Client client;
	file.open("visits.dat",ios::in|ios::binary|ios::out);
	if(file){
		while(file.read((char*)&data,sizeof(data))){
			visit.setData(data);
			client=visit.getClient();
			if(client.getName()==name){
				found=true;
				if(toDelete){
					data.intervention_num=0;//sets id to 0 (id with value 0 is marked as deleted)
				}else{
					getUpdateDataForVisit(&data);
				}
				//goes back 1 record, writes new data over old data
				positionCursor(&file,sizeof(data));
				file.write((char*)&data,sizeof(data));
				break;
			}
		}
	}
	if(!found){
		printLogo("error.txt");
		cout<<"\n\n\n\t\t\t\t\t\tERROR: NO VISIT WAS FOUND WITH THE GIVEN INTERVENTION NUMBER."<<endl;
	}else{
		printLogo("success.txt");
		cout<<"\n\n\n\t\t\t\t\t\tINFORMATION WAS "<<((toDelete)? "DELETED":"UPDATED")<<" SUCCESSFULLY.\n";
	}
	getch();
	wait();
}
void Database::updateRemoval(bool toDelete){
	int temp;
	int id;
	string name;
	Removal removal;
	string options[3]={"[ INTERVENTION # ]","[  CLIENT  NAME  ]","[    GO  BACK    ]"};
	string instructions="\n\n\t\t\t\t\t\t\t     SEARCH REMOVAL\n\n\t\t\t\t\t\tSELECT THE TYPE OF INFORMATION YOU WISH TO PROVIDE\n\n";
	Menu menu(options,3,instructions);
	temp=menu.prompt();
	printLogo("logo.txt");
	if(temp!=2){
		getInput(temp,id,name);
		if(temp==0){
			updateRByID(id,toDelete);
		}else{
			updateRByName(name,toDelete);
		}
	}
}
void Database::updateRByID(int id,bool toDelete){
	fstream file;
	bool found=false;
	REMOVALDATA data;
	Removal removal;
	file.open("removals.dat",ios::in|ios::binary|ios::out);
	if(file){
		while(file.read((char*)&data,sizeof(data))){
			if(data.intervention_num==id){
				found=true;
				if(toDelete){
					data.intervention_num=0;
				}else{
					getUpdateDataForRemoval(&data);
				}
				//goes back 1 record, writes new data over old data
				positionCursor(&file,sizeof(data));
				file.write((char*)&data,sizeof(data));
				break;
			}
		}
	}
	if(!found){
		printLogo("error.txt");
		cout<<"\n\n\n\t\t\t\t\t\tERROR: NO REMOVAL WAS FOUND WITH THE GIVEN INTERVENTION NUMBER."<<endl;
	}else{
		printLogo("success.txt");
		cout<<"\n\n\n\t\t\t\t\t\tINFORMATION WAS "<<((toDelete)? "DELETED":"UPDATED")<<" SUCCESSFULLY.\n";
	}
	getch();
	wait();
}
void Database::updateRByName(string name,bool toDelete=false){
	fstream file;
	Visit visit;
	bool found=false;
	Client client;
	REMOVALDATA data;
	file.open("visits.dat",ios::in|ios::binary|ios::out);
	if(file){
		while(file.read((char*)&data,sizeof(data))){
			visit.setData(data);
			client=visit.getClient();
			if(client.getName()==name){
				found=true;
				if(toDelete){
					data.intervention_num=0;//marked as deleted
				}else{
					getUpdateDataForRemoval(&data);
				}
				//goes back 1 record, writes new data over old data
				positionCursor(&file,sizeof(data));
				file.write((char*)&data,sizeof(data));
				break;
			}
		}
	}
	if(!found){
		printLogo("error.txt");
		cout<<"\n\n\n\t\t\t\t\t\tERROR: NO VISIT WAS FOUND WITH THE GIVEN CLIENT NAME."<<endl;
	}else{
		printLogo("success.txt");
		cout<<"\n\n\n\t\t\t\t\t\tINFORMATION WAS "<<((toDelete)? "DELETED":"UPDATED ")<<"SUCCESSFULLY.\n";
	}
	getch();
	wait();
}
void Database::getUpdateDataForRemoval(REMOVALDATA *data){
	int indicator;
	string adoptedOptions[2]={"[ YES ]","[ NO  ]"};
	string adoptedInstructions="\n\n\n\t\t\t\t\tIS THE ANIMAL ADOPTED ?\n";
	Menu adoptedMenu(adoptedOptions,2,adoptedInstructions);
	int temp;//adopted menu indicator
	while(indicator!=3){
		printLogo("logo.txt");
		cout<<"\n\n\n\t\t\t\tENTER THE CORRESPONDING NUMBER TO UPDATE THE INFORMATION or 3 to SAVE\n\n";
		cout<<"\n\t\t\t\t\t1. ADOPTED: "<<((data->isAdopted)? "YES":"NO")<<endl;
		cout<<"\t\t\t\t\t2. OUTCOME: "<<data->location<<endl;
		cout<<"\t\t\t\t\t3. SAVE"<<endl;
		cout<<"\n\n\t\t\t\t\tENTER: ";
		indicator=getch();
		indicator-=48;
		switch(indicator){
			case 1:temp=adoptedMenu.prompt();
					if(temp==0){
						data->isAdopted=true;
					}else{
						data->isAdopted=false;
					}
				break;
			case 2:
				cout<<"\n\n\t\t\t\t\t\tENTER THE UPDATED OUTCOME: ";
				cin.getline(data->outcome,40);
		}
	}
}
void Database::getUpdateDataForVisit(VISITDATA *data){
	int indicator;
	while(indicator!=3){
		printLogo("logo.txt");
		cout<<"\n\n\n\t\t\t\tENTER THE CORRESPONDING NUMBER TO UPDATE THE INFORMATION or 3 to SAVE\n\n";
		cout<<"\n\t\t\t\t\t1. REASON: "<<data->reason<<endl;
		cout<<"\t\t\t\t\t2. LOCATION: "<<data->location<<endl;
		cout<<"\t\t\t\t\t3. SAVE"<<endl;
		cout<<"\n\n\t\t\t\t\tENTER: ";
		indicator=getch();
		indicator-=48;
		switch(indicator){
			case 1:
				cout<<"\n\t\t\t\t\t\tENTER UPDATED REASON: ";
				cin.getline(data->reason,40);
				break;
			case 2:
				getUpdatedLocation(data);
		}
	}
}
void Database::getUpdatedLocation(VISITDATA *data){
	string locations[14]={"   KINGSTON    ","  ST. ANDREW   ","   PORTLAND    ","  ST. THOMAS   ","   ST. MARY    ",
					" ST. CATHERINE ","    ST. ANN    ","   MANCHESTER  ","   CLARENDON   ","    HANOVER    ",
					" ST. ELIZABETH ","   ST. JAMES   ","    TRELAWNY   ","  WESTMORELAND "
				};
	string instructions="\n\n\t\t\t\t\tSELECT THE LOCATION WHERE THE VISIT WILL BE TAKING PLACE\n\n";
	Menu menu(locations,14,instructions);
	int indicator=menu.prompt();
	strcpy(data->location,locations[indicator].c_str());
}