#ifndef ANIMAL_H
#define ANIMAL_H
#include <iostream>
#include <string>
#include <cstring>
using std::string;

typedef struct ani_container{
	int key;
	int ownerID;
	char name[25];
	char type[25];
	char breed[25];
	char gender[25];
	int age;
}ANIMALDATA;

class Animal{
private:
	int key;
	int ownerID;
	string name;
	string type;
	string breed;
	string gender;
	int age;
	ANIMALDATA container;
public:

	Animal(string name,string type,string breed,string gender,int age){
		key=0;
		ownerID=0;
		this->name=name;
		this->type=type;
		this->breed=breed;
		this->gender=gender;
		this->age=age;
		updateContainer();
	}
	Animal(){
		key=0;
		ownerID=0;
		name="";
		type="";
		breed="";
		gender="";
		age=0;
		updateContainer();
	}
	Animal(Animal *ptr){
		this->key=ptr->getKey();
		this->ownerID=ptr->getOwnerID();
		this->name=ptr->getName();
		this->type=ptr->getType();
		this->breed=ptr->getBreed();
		this->gender=ptr->getGender();
		this->age=ptr->getAge();
		updateContainer();
	}
	Animal(ANIMALDATA container){
		key=container.key;
		ownerID=container.ownerID;
		name=container.name;
		type=container.type;
		breed=container.breed;
		gender=container.gender;
		age=container.age;
		//updateContainer();
		this->container=container;
	}
	void updateContainer(){
		container.key=this->key;
		container.ownerID=this->ownerID;
		strcpy(container.name,this->name.c_str());
		strcpy(container.type,this->type.c_str());
		strcpy(container.breed,this->breed.c_str());
		strcpy(container.gender,this->gender.c_str());
		container.age = this->age;
	}
	void setData(ANIMALDATA container){
		key=container.key;
		ownerID=container.ownerID;
		name=container.name;
		type=container.type;
		breed=container.breed;
		gender=container.gender;
		age=container.age;
		//updateContainer();
		this->container=container;
	}
	ANIMALDATA getData(){
		return container;
	}
	void setKey(int key);
	int getKey();
	void setOwnerID(int id);
	int getOwnerID();
	void setName(string name);
	void setType(string type);
	void setBreed(string breed);
	void setGender(string gender);
	void setAge(int age);
	string getName();
	string getType();
	string getBreed();
	string getGender();
	int getAge();
	void display();
	//void save();
	~Animal(){}
};

#endif
