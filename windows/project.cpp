#include <iostream>
#include "Menu.h"
#include "Database.h"
#include "Report.h"

using namespace std;

Visit newVisit();
void getVisitLocation(string &text);
Removal newRemoval();
Client newClient();
void getAbilityToMakePayment(string &text);
Animal newAnimal();
void setGender(string& gender,string type);
void request(int indicator);
void report_category();
int report_type(int indicator);

int main(){
	int indicator=0;
	printLogo("logo.txt");
	string options[4]={"[VETERINARY CLINIC VISIT ]","[     REMOVAL REQUEST    ]","[    GENERATE REPORT     ]","[          EXIT          ]"};
	string instructions="\n\n\t\t\t\t\t\t\t\t  MAIN MENU\n\n\t\t\t\t\t********>>>****>>***  USE ARROW KEYS TO NAVIGATE  ***<<****<<<********\n\n\n";
	Menu main_menu(options,4,instructions);
	while(indicator!=3){
		indicator=main_menu.prompt();
		switch(indicator){
			case 0:
			case 1:
				request(indicator);
				break;
			case 2:
				report_category();
				break;
		}
	}
	return 0;
}
Animal newAnimal(){
	int indicator=0;
	string name;
	string type;
	string breed;
	string gender;
	int age;
	printLogo("logo.txt");
	cout<<"\n\n\n\t\t\t\t\tANY ERRORS MADE BY YOU CAN BE CORRECTED AT THE END.";
	cout<<"\n\n\n\t\t\t\t\tENTER THE ANIMAL'S NAME: ";
	getline(cin,name);
	cout<<"\n\n\t\t\t\t\tENTER THE TYPE OF ANIMAL: ";
	getline(cin,type);
	cout<<"\n\n\t\t\t\t\tENTER THE BREED: ";
	getline(cin,breed);
	setGender(gender,type);
	cout<<"\n\n\t\t\t\t\tENTER THE "+type+"'s AGE: ";
	cin>>age;
	getch();
	while(!(indicator==6)){
		printLogo("logo.txt");
		cout<<"\n\n\t\t\t\tPRESS THE CORRESPONDING NUMBER TO CORRECT ANY INFORMATION OR  6 TO CONTINUE"<<endl;
		cout<<"\n\t\t\t\t\t1. NAME: "<<name<<endl;
		cout<<"\t\t\t\t\t2. TYPE: "<<type<<endl;
		cout<<"\t\t\t\t\t3. BREED: "<<breed<<endl;
		cout<<"\t\t\t\t\t4. GENDER: "<<gender<<endl;
		cout<<"\t\t\t\t\t5. AGE: "<<age<<endl;
		cout<<"\t\t\t\t\t6. CONTINUE"<<endl;
		cout<<"\n\n\t\t\t\t\tENTER:";
		indicator=getch();
		indicator-=48;
		if(indicator==5){
			cout<<"\n\n\t\t\t\t\t\tENTER CORRECT AGE: ";
			cin>>age;
		}else if(indicator==4){
			setGender(gender,type);
		}else if(indicator<=3 && indicator>=1){
			cout<<"\n\n\t\t\t\t\t\tENTER CORRECT INFO: ";
			switch(indicator){
				case 1:
					getline(cin,name);
					break;
				case 2:
					getline(cin,type);
					break;
				case 3:
					getline(cin,breed);
					break;
			}
		}
	}
	Animal animal(name,type,breed,gender,age);
	return animal;
}
void setGender(string& gender,string type){
	string genders[2]={"[  MALE  ]","[ FEMALE ]"};
	string instructions="\n\n\t\t\t\t\t\t     SELECT THE "+type+"'s GENDER\n\n\n";
	Menu genderMenu(genders,2,instructions);
	switch(genderMenu.prompt()){
		case 0:
			gender="MALE";
			break;
		case 1:
			gender="FEMALE";
			break;
	}
}
Client newClient(){
	int indicator=0;
	string firstName;
	string lastName;
	string contactNumber;
	string abilityToMakePayment;
	float deposit=0.0;
	Animal animal;
	printLogo("logo.txt");
	cout<<"\n\n\t\t\t\tANY ERRORS MADE BY YOU CAN BE CORRECTED AT THE END.";
	cout<<"\n\n\n\t\t\t\t\tENTER THE CLIENT'S FIRST NAME: ";
	getline(cin,firstName);
	cout<<"\n\t\t\t\t\tENTER THE CLIENT'S LAST NAME: ";
	getline(cin,lastName);
	cout<<"\n\t\t\t\t\tENTER THE CLIENT'S CONTACT NUMBER: ";
	getline(cin,contactNumber);
	getAbilityToMakePayment(abilityToMakePayment);
	if(abilityToMakePayment=="ABLE" ||abilityToMakePayment=="DONATION"){
		cout<<"\n\n\t\t\t\t\tENTER THE CLIENT'S DEPOSIT: ";
		cin>>deposit;
		getch();
	}
	while(!(indicator==6)){
		printLogo("logo.txt");
		cout<<"\n\n\t\t\t\tPRESS THE CORRESPONDING NUMBER TO CORRECT ANY INFORMATION OR  6 TO CONTINUE"<<endl;
		cout<<"\n\n\t\t\t\t\t1. FIRST NAME: "<<firstName<<endl;
		cout<<"\t\t\t\t\t2. LAST NAME: "<<lastName<<endl;
		cout<<"\t\t\t\t\t3. CONTACT NUMBER: "<<contactNumber<<endl;
		cout<<"\t\t\t\t\t4. ABILITY TO MAKE PAYMENT: "<<abilityToMakePayment<<endl;
		if(abilityToMakePayment=="ABLE" || abilityToMakePayment=="DONATION"){
			cout<<"\t\t\t\t\t5. DEPOSIT: "<<deposit<<endl;
		}
		cout<<"\t\t\t\t\t6. CONTINUE";
		cout<<"\n\n\t\t\t\t\tENTER:";
		indicator=getch();
		indicator-=48;
		switch(indicator){
			case 1:
				cout<<"\n\t\t\t\t\tENTER FIRST NAME: ";
				getline(cin,firstName);
				break;
			case 2:
				cout<<"\n\t\t\t\t\tENTER LAST NAME: ";
				getline(cin,lastName);
				break;
			case 3:
				cout<<"\n\t\t\t\t\tENTER CONTACT NUMBER: ";
				getline(cin,contactNumber);
				break;
			case 4:
				getAbilityToMakePayment(abilityToMakePayment);
				break;
			case 5:
				if(abilityToMakePayment!="ABLE" && abilityToMakePayment!="DONATION"){
					getAbilityToMakePayment(abilityToMakePayment);
					if(abilityToMakePayment=="UNABLE"){
						deposit=0;
					}
				}else{
					cout<<"\n\t\t\t\t\tENTER THE CLIENT'S DEPOSIT: ";
					cin>>deposit;
				}
		}
	}
	animal=newAnimal();
	Client client(firstName,lastName,contactNumber,abilityToMakePayment,deposit,animal);
	return client;
}
void getAbilityToMakePayment(string &text){
	string options[3]={"[   ABLE   ]","[  UNABLE  ]","[ DONATION ]"};
	string instructions="\n\n\t\t\t\t\t\t   CLIENT'S ABILITY TO MAKE PAYMENT\n\n\n";
	Menu menu(options,3,instructions);
	switch(menu.prompt()){
		case 0:
			text="ABLE";
			break;
		case 1:
			text="UNABLE";
			break;
		case 2:
			text="DONATION";
			break;
	}
}
Visit newVisit(){
	Client client=newClient();
	string location;
	string reason;
	getVisitLocation(location);
	cout<<"\n\n\t\t\t\t\t\tENTER THE REASON FOR THIS VISIT: ";
	getline(cin,reason);
	Visit visit(client,reason,location);
	return visit;

}
void getVisitLocation(string &text){
	string locations[14]={"   KINGSTON    ","  ST. ANDREW   ","   PORTLAND    ","  ST. THOMAS   ","   ST. MARY    ",
					" ST. CATHERINE ","    ST. ANN    ","   MANCHESTER  ","   CLARENDON   ","    HANOVER    ",
					" ST. ELIZABETH ","   ST. JAMES   ","    TRELAWNY   ","  WESTMORELAND "
				};
	string instructions="\n\n\t\t\t\t\tSELECT THE LOCATION WHERE THE VISIT WILL BE TAKING PLACE\n\n";
	Menu menu(locations,14,instructions);
	int indicator=menu.prompt();
	text=locations[indicator];
}
Removal newRemoval(){
	Client client=newClient();
	string address;
	printLogo("logo.txt");
	cout<<"\n\n\t\t\t\t\tENTER THE ADDRESS WHERE REMOVAL WILL TAKE PLACE: ";
	getline(cin,address);
	Removal removal(client,address);
	return removal;
}
/////////////////////////////////////////////////////////////////////////////////////
void request(int indicator){
	int action;
	Database db;
	string type;
	if(indicator==0){
		type="\t\t\t\t\t\t\tVETERINARY CLINIC VISIT";
	}else{
		type="\t\t\t\t\t\t\t     REMOVAL REQUEST";
	}
	string options[6]={"[   ADD    ]","[  UPDATE  ]","[   VIEW   ]","[ VIEW ALL ]","[  DELETE  ]","[  GO BACK ]"};
	string instructions="\n\n "+type+"\n\n\t\t\t\t\t\t PLEASE SELECT AN ACTION YOU WISH TO TAKE\n\n\n\n";
	Menu menu(options,6,instructions);
	action=menu.prompt();
	if(indicator==0){
		if(action==0){
			db.addVisit(newVisit());
		}else if(action==1){
			db.update(indicator);
		}else if(action==2){
			db.view(0);
		}else if(action==3){
			db.viewAll("visit");
		}else if(action==4){
			db.update(indicator,true);
		}
	}else if(indicator==1){
		if(action==0){
			db.addRemoval(newRemoval());
		}else if(action==1){
			db.update(indicator);
		}else if(action==2){
			db.view(1);
		}else if(action==3){
			db.viewAll("removal");
		}else if(action==4){
			db.update(indicator,true);
		}
	}
}
void report_category(){
	int exit;
	int indicator2;
	int indicator;
	string options1[3]={"[  VETERINARY CLINIC VISIT  ]","[      REMOVAL REQUEST      ]","[          GO BACK          ]"};
	string instructions="\n\n\t\t\t\t\t\t\t\tREPORT CATEGORY\n\n\t\t\t\t\t   PLEASE SELECT THE CATEGORY OF THE REPORT YOU WISH TO GENERATE\n\n\n";
	Menu type_menu(options1,3,instructions);
	while(indicator!=2){
		indicator=type_menu.prompt();
		if(indicator==2){
			break;
		}
		exit=report_type(indicator);
		if(exit==4){
			break;//back to main menu
		}
	}
}
int report_type(int indicator){
	int temp;
	Report rp(indicator);
	string instructions="\n\n\t\t\t\t\t\t\t       REPORT MENU\n\n\t\t\t\t\t\t   SELECT THE DESIRED TYPE OF REPORT\n\n\n";
	string visit_options[5]={"[  LOCATION  ]","[ VISIT DATE ]","[   ANIMAL   ]","[  GO BACK   ]","[  MAIN MENU ]"};
	string removal_options[5]={"[    LOCATION     ]","[  REQUEST DATE   ]","[ REMOVAL OUTCOME ]","[     GO BACK     ]","[    MAIN MENU    ]"};
	Menu menu(5,instructions);
	if (indicator==0){
		menu.setOptions(visit_options);
	}else{
		menu.setOptions(removal_options);
	}
	temp=menu.prompt();
	if(temp==0){
		rp.locationReport();//both type of request share location attribute(address and parish)
	}else if(temp==1){
		rp.dateReport();
	}else if(temp==2){
		if(indicator==0){
			rp.AnimalReport();
		}else if(indicator==1){
			rp.outcomeReport();
		}
	}
	return temp;
}
