#include <iostream>
#include "Client.h"
using std::cout;
using std::endl;
using std::string;

void Client::setData(CLIENTDATA data){
	key=data.key;
	firstName=data.firstName;
	lastName=data.lastName;
	contactNumber=data.contactNumber;
	abilityToMakePayment=data.abilityToMakePayment;
	deposit=data.deposit;
	animal.setData(data.animal_data);
	//updateData();
	this->data=data;
}
void Client::setAbilityToMakePayment(string text){
	abilityToMakePayment=text;
	strcpy(data.abilityToMakePayment,abilityToMakePayment.c_str());
}
string Client::getAbilityToMakePayment(){
	return abilityToMakePayment;
}
void Client::setKey(int key){
	this->key=key;
	data.key=key;
}
int Client::getKey(){
	return key;
}
void Client::makeDeposit(float amount){
	deposit+=amount;
	data.deposit=deposit;
}
string Client::getContact(){
	return contactNumber;
}
string Client::getName(){
	string fullname=firstName+" "+lastName;
	return fullname;
}
void Client::setContact(string number){
		contactNumber=number;
		strcpy(data.contactNumber,contactNumber.c_str());
}
void Client::setName(string firstName,string lastName){
		this->firstName=firstName;
		this->lastName=lastName;
		strcpy(data.firstName,firstName.c_str());
		strcpy(data.lastName,lastName.c_str());
}
void Client::display(){
	cout<<"\n\t\t\t\tCLIENT DETAILS:"<<endl;
	cout<<"\t\t\t\t\t\t\tNAME: "<<getName()<<endl;
	cout<<"\t\t\t\t\t\t\tCONTACT NUMBER: "<<contactNumber<<endl;
	cout<<"\t\t\t\t\t\t\tABILITY TO MAKE PAYMENT: "<<abilityToMakePayment<<endl;
	cout<<"\t\t\t\t\t\t\tPAYMENT DEPOSIT: "<<deposit<<endl;
	cout<<"\n\t\t\t\tANIMAL DETAILS: "<<endl;
	animal.display();
}
float Client::getDeposit(){
	return deposit;
}
