#include "Database.h"
#include <iostream>
#include <string>
#include <cstring>
#include "Report.h"
#include <algorithm>
#include <fstream>
#include <vector>

using namespace std;

void Report::printHeader(){
	printLogo("reportlogo.txt");
}
void Report::getLocation(string& text){
	string locations[14]={"   KINGSTON    ","  ST. ANDREW   ","   PORTLAND    ","  ST. THOMAS   ","   ST. MARY    ",
					" ST. CATHERINE ","    ST. ANN    ","   MANCHESTER  ","   CLARENDON   ","    HANOVER    ",
					" ST. ELIZABETH ","   ST. JAMES   ","    TRELAWNY   ","  WESTMORELAND "
				};
	string instructions="\n\n\t\t\t\t\t\t    SELECT THE LOCATION FOR THE REPORT\n\n";
	Menu menu(locations,14,instructions);
	int indicator=menu.prompt();
	text=locations[indicator];
}
void Report::locationReport(){
	string location;
	string date;
	string animalType;
	string outcome;
	getLocation(location);
	printHeader();
	displayRecords(location,date,animalType,outcome);
	cout<<"\n\t\t\t\tTYPE OF REPORT: "<<type<<endl;
	cout<<"\n\t\t\t\tTHIS "<<type<<" REPORT WAS GENERATED ON "<<date_time<<endl;
	getch();

}
void Report::displayRecords(string location,string date,string animalType,string outcome){
	ifstream file;
	VISITDATA vdata;
	REMOVALDATA rdata;
	ANIMALDATA animalData;
	string filename;
	int count=0;
	if(type=="VISIT"){
		filename="visits.dat";
	}else{
		filename="removals.dat";
	}
	file.open(filename.c_str(),ios::in|ios::binary);
	if(type=="VISIT"){
		if(file){
			while(file.read((char*)&vdata,sizeof(vdata))){
				if(vdata.intervention_num==0)/*if record is marked as deleted*/{
					continue;
				}
				cout<<"\n\n\n";
				if(location!=""){
					if(strcmp(location.c_str(),vdata.location)==0){
						displaySingleVisit(vdata);
						count++;
					}
				}else if(date!=""){
					if(strcmp(date.c_str(),vdata.date_time)==0){
						displaySingleVisit(vdata);
						count++;
					}
				}else if(animalType!=""){
					animalData=vdata.clientdata.animal_data;
					if(strcmp(animalType.c_str(),animalData.type)==0){
						displaySingleVisit(vdata);
						count++;
					}
				}
			}
			cout<<"\n\t\t\t\tTOTAL NUMBER OF RECORDS FOUND: "<<count<<"\n";
		}else{
			printLogo("error.txt");
			cout<<"\n\n\n\t\t\t\t\t\tERROR: THERE ARE NO RECORDS, REPORT GENERATION FAILED"<<endl;
			getch();
			getch();
		}
	}else if(type=="REMOVAL"){
		if(file){
			while(file.read((char*)&rdata,sizeof(rdata))){
				if(rdata.intervention_num==0){
					continue;
				}
				cout<<"\n\n\n";
				if(location!=""){
					if(strcmp(location.c_str(),rdata.location)==0){
						displaySingleRemoval(rdata);
						count++;
					}
				}else if(date!=""){
					if(strcmp(date.c_str(),rdata.date_time)==0){
						displaySingleRemoval(rdata);
						count++;
					}

				}else if(outcome!=""){
					if(strcmp(outcome.c_str(),rdata.outcome)==0){
						displaySingleRemoval(rdata);
						count++;
					}
				}
			}
			cout<<"\n\t\t\t\tTOTAL NUMBER OF RECORDS FOUND: "<<count<<"\n";
		}else{
			printLogo("error.txt");
			cout<<"\n\n\n\t\t\t\t\t\tERROR: THERE ARE NO RECORDS, REPORT GENERATION FAILED"<<endl;
		}
		getch();
		getch();
	}
}
void Report::displaySingleVisit(VISITDATA data){
	Visit visit(data);
	Client client=visit.getClient();
	cout<<"\t\t    ID: "<<visit.getInterventionNum()<<"   LOCATION:"<<visit.getLocation()<<"   CLIENT: "<<client.getName()<<"   DATE: "<<visit.getDate()<<"\n\n";
}
void Report::displaySingleRemoval(REMOVALDATA data){
	Removal removal(data);
	Client client=removal.getClient();
	cout<<"\t\t    ID: "<<removal.getInterventionNum()<<"   OUTCOME:"<<removal.getOutcome()<<"   CLIENT: "<<client.getName()<<"   DATE: "<<removal.getDate()<<"\n\n";
}
void Report::outcomeReport(){
	string location;
	string date;
	string animalType;
	string outcome;
	printLogo("logo.txt");
	cout<<"\n\n\n\t\t\t\t\tENTER THE REMOVAL OUTCOME FOR THE REPORT:";
	getline(cin,outcome);
	printHeader();
	displayRecords(location,date,animalType,outcome);
}
string Report::getDate(){
	vector<string> dates;
	fstream file;
	VISITDATA vdata;
	REMOVALDATA rdata;
	Visit visit;
	Removal removal;
	if(type=="VISIT"){
		file.open("visits.dat",ios::in|ios::binary);
		if(!file){
			printLogo("error.txt");
			cout<<"\n\n\n\t\t\t\t\tERROR OPENING FILE: THERE WERE NO RECORDS FOUND\n";
			getch();
			return "error";
		}else{
			while(file.read((char*)&vdata,sizeof(vdata))){
				if(vdata.intervention_num==0){
					continue;
				}
				visit.setData(vdata);
				if(find(dates.begin(),dates.end(),visit.getDate())==dates.end()){
					dates.push_back(visit.getDate());
				}
			}
		}
	}else if(type=="REMOVAL"){
		file.open("removals.dat",ios::in|ios::binary);
		if(!file){
			printLogo("error.txt");
			cout<<"\n\n\n\t\t\t\t\tERROR OPENING FILE: THERE WERE NO RECORDS FOUND\n";
			getch();
			return "error";
		}else{
			while(file.read((char*)&rdata,sizeof(rdata))){
				if(rdata.intervention_num==0){
					continue;
				}
				removal.setData(rdata);
				if(find(dates.begin(),dates.end(),removal.getDate())==dates.end()){
					dates.push_back(removal.getDate());
				}
			}
		}
	}
	int size=dates.size();//size of vector
	string date_array[size];

	//populate date array so it can be passed to menu
	for(int i=0;i<size;i++){
		date_array[i]=dates[i];
	}
	string instructions="\n\n\n\t\t\t\t\t\tSELECT THE DATE THAT IS WANTED FOR THE REPORT\n\n";
	Menu menu(date_array,size,instructions);
	int indicator=menu.prompt();
	return dates[indicator];
}
string Report::getAnimal(){
	ANIMALDATA animal_data;
	Animal animal;
	VISITDATA visit_data;
	REMOVALDATA removal_data;
	vector<string> animals;
	fstream file;
	if(type=="VISIT"){
		file.open("visits.dat",ios::in|ios::binary);
		if(!file){
			printLogo("error.txt");
			cout<<"\n\n\n\t\t\t\t\tERROR OPENING FILE: THERE WERE NO RECORDS FOUND\n";
			getch();
			return "error";
		}else{
			while(file.read((char*)&visit_data,sizeof(visit_data))){
				if(visit_data.intervention_num==0){
					continue;
				}
				animal_data=visit_data.clientdata.animal_data;
				animal.setData(animal_data);
				if(find(animals.begin(),animals.end(),animal.getType())==animals.end()){
					animals.push_back(animal.getType());
				}
			}
		}
	}else if(type=="REMOVAL"){
		file.open("removals.dat",ios::in|ios::binary);
		if(!file){
			cout<<"\n\n\n\t\t\t\t\tERROR OPENING FILE: THERE WERE NO RECORDS FOUND\n";
			getch();
			return "error";
		}else{
			while(file.read((char*)&removal_data,sizeof(removal_data))){
				if(removal_data.intervention_num==0){
					continue;
				}
				animal_data=removal_data.clientdata.animal_data;
				animal.setData(animal_data);
				if(find(animals.begin(),animals.end(),animal.getType())==animals.end()){
					animals.push_back(animal.getType());
				}
			}
		}
	}
	int size = animals.size();//size of vector
	string animal_array[size];
	for(int i=0;i<size;i++){
		animal_array[i]=animals[i];
	}
	string instructions="\n\n\n\t\t\t\t\t\tSELECT THE ANIMAL TYPE FOR THE REPORT\n\n";
	Menu menu(animal_array,size,instructions);
	int indicator=menu.prompt();
	return animal_array[indicator];
}
void Report::AnimalReport(){
	string animalType=getAnimal();
	if(animalType!="error"){
		string location;
		string date;
		string outcome;
		printHeader();
		displayRecords(location,date,animalType,outcome);
		getch();
	}
}
void Report::dateReport(){
	string date=getDate();
	if(date!="error"){
		string location;
		string animalType;
		string outcome;
		printHeader();
		displayRecords(location,date,animalType,outcome);
	}
}

