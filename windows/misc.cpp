#include "misc.h"
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#if (defined (_WIN32) || defined (_WIN64))
	#include <conio.h>//file that contains getch on windows
#elif (defined (LINUX) || defined (__linux__))
	#include <termios.h>
	#include <unistd.h>
#endif

using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::ifstream;

void printLogo(string filename){
	ifstream fileob;
	fileob.open(filename.c_str());
	string line;
	if(!fileob.is_open()){
		cout<<"\n\n\n\n\t\t\t\t\tERROR: FILE COULD NOT BE OPENED."<<endl;
	}else{
		#if (defined (_WIN32) || defined (_WIN64))
			system("cls");
		#elif (defined (LINUX) || defined (__linux__))
			system("clear");
		#endif
		while(!fileob.eof()){
			getline(fileob,line);
			cout<<line<<endl;
		}
		fileob.close();
	}
}
void wait(){
	getch();
}
#if  (defined (LINUX) || defined (__linux__))
int getch(void)
{
 int ch;
 struct termios oldt;
 struct termios newt;
 tcgetattr(STDIN_FILENO, &oldt); /*store old settings */
 newt = oldt; /* copy old settings to new settings */
 newt.c_lflag &= ~(ICANON | ECHO); /* make one change to old settings in new settings */
 tcsetattr(STDIN_FILENO, TCSANOW, &newt); /*apply the new settings immediatly */
 ch = getchar(); /* standard getchar call */
 tcsetattr(STDIN_FILENO, TCSANOW, &oldt); /*reapply the old settings */
 return ch; /*return received char */
}
#endif
