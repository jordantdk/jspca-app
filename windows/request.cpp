#include <iostream>
#include "Request.h"
#include "Client.h"
using std::cout;
using std::endl;
using std::string;

void Request::display(){
	cout<<"\t\t\t\tClient details:\n"<<endl;
	client.display();
	cout<<"\n\t\t\t\tREQUEST DETAILS:\n"<<endl;
	cout<<"\t\t\t\t\t\t\tDATE: "<<date_time;
	cout<<"\t\t\t\t\t\t\tINTERVENTION NUMBER: "<<interventionNum<<endl;
}
int Request::getInterventionNum(){
		return interventionNum;
}
void Request::setInterventionNum(int num){
	this->interventionNum=num;
}
string Request::getDate(){
		return date_time;
}
Client Request::getClient(){
		return client;
}
void Request::setClient(Client client){
		this->client=client;
}