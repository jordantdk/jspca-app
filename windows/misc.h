#ifndef MISC_H
#define MISC_H
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>

#define UP 72
#define DOWN 80
#define LEFT 75
#define RIGHT 77

using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::ifstream;

void printLogo(string filename);
void wait();
#if (defined (_WIN32) || defined (_WIN64))
	#include <conio.h>//file that contains getch on windows
#elif (defined (LINUX) || defined (__linux__))
	#include <termios.h>
	#include <unistd.h>
  int getch(void);
#endif

#endif
