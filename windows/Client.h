#ifndef CLIENT_H
#define CLIENT_H
#include <iostream>
#include <string>
#include <cstring>
#include "Animal.h"
using std::string;

typedef struct cli_container{
	int key;
	char firstName[25];
	char lastName[25];
	char contactNumber[14];
	char abilityToMakePayment[10];
	float deposit;
	ANIMALDATA animal_data;
}CLIENTDATA;

class Client{
private:
	int key;
	string firstName;
	string lastName;
	string contactNumber;
	string abilityToMakePayment;
	float deposit;
	Animal animal;
	CLIENTDATA data;
public:
	Client(string firstName,string lastName,string contactNumber,string abilityToMakePayment,float deposit,Animal animal){
		key=0;
		this->firstName=firstName;
		this->lastName=lastName;
		this->contactNumber="(876)"+contactNumber;
		this->abilityToMakePayment=abilityToMakePayment;
		this->deposit=deposit;
		this->animal=animal;
		updateDATA();
	}
	Client(){
		key=0;
		this->firstName="";
		this->lastName="";
		this->contactNumber="";
		this->abilityToMakePayment="";
		deposit=0.0;
		updateDATA();
	}
	Client(CLIENTDATA data){
		key=data.key;
		firstName=data.firstName;
		lastName=data.lastName;
		contactNumber=data.contactNumber;
		abilityToMakePayment=data.abilityToMakePayment;
		deposit=data.deposit;
		animal.setData(data.animal_data);
		//updateDATA();
		this->data=data;
	}
	void updateDATA(){
		data.key=key;
		strcpy(data.firstName,firstName.c_str());
		strcpy(data.lastName,lastName.c_str());
		strcpy(data.contactNumber,contactNumber.c_str());
		strcpy(data.abilityToMakePayment,abilityToMakePayment.c_str());
		data.deposit=deposit;
		data.animal_data=animal.getData();
	}
	CLIENTDATA getData(){
		return data;
	}
	void setData(CLIENTDATA data);
	int getKey();
	void setKey(int key);
	void setAbilityToMakePayment(string text);
	string getAbilityToMakePayment();
	void setAnimal(Animal animal);
	void setName(string firstName,string lastName);
	void setContact(string number);
	Animal getAnimal();
	string getName();
	string getContact();
	float getDeposit();
	void makeDeposit(float amount);
	void display();
	~Client(){
		/*
		firstName=firstName.c_str();
		lastName=lastName.c_str();
		contactNumber=contactNumber.c_str();
		abilityToMakePayment=abilityToMakePayment.c_str();
		*/
	}

};

#endif
