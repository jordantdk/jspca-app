#ifndef DATABASE_H
#define DATABASE_H
#include <iostream>
#include "Visit.h"
#include "Removal.h"
#include "Menu.h"
#include <string>
#include <fstream>

using std::fstream;
using std::string;

class Database{
private:
	fstream file;
public:

	void viewAll(string objectName);
	int getNextID(string filename,int obj_size);
	void addVisit(Visit object);
	void addRemoval(Removal object);
	void viewVisit();
	void viewRemoval();
	void view(int indicator);
	void viewClient(int type);
	void viewCByID(int id,int type);
	void viewCByName(string name,int type);
	void viewCByContact(string contact,int type);
	void viewVByID(int id);
	void viewVbyName(string name);
	void viewRByID(int id);
	void viewRByName(string name);
	void getInput(int temp,int &id,string &name);
	void update(int indicator,bool toDelete=false);
	//////////////////////////////////
	void updateAnimal(ANIMALDATA *adata);
	void updateClient(int type,bool toDelete);
	void updateC(int id,string name,string contact,int type,bool toDelete);
	void getUpdateDataForC(CLIENTDATA *cdata);
	void getAbilityToMakePayment(CLIENTDATA *cdata);
	void updateVisit(bool toDelete);
	void updateRemoval(bool toDelete);
	void updateVByID(int id,bool toDelete);
	void updateVByName(string name,bool toDelete);
	void updateRByID(int id,bool toDelete);
	void updateRByName(string name,bool toDelete);
	void getUpdateDataForRemoval(REMOVALDATA *data);
	void getUpdateDataForVisit(VISITDATA *data);
	void getUpdatedLocation(VISITDATA *data);
	void positionCursor(fstream *file,int size);
};
#endif
