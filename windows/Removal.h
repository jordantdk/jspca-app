#ifndef REMOVAL_H
#define REMOVAL_H
#include <iostream>
#include <string>
#include <cstring>
#include "Request.h"
#include "Visit.h"
using std::string;

typedef class rem_container:public vi_data{
public:
	//int intervention_num;	inherited
	//char date_time[30];	inherited
	char address[50];
	bool isAdopted;
	char outcome[40];
	//CLIENTDATA clientdata;	inherited
}REMOVALDATA;

class Removal: public Request{
private:
	string address;
	bool isAdopted;
	string outcome;
	REMOVALDATA data;
public:
	Removal(){
		address="";
		isAdopted=false;
		outcome="";
		updateData();
	}
	Removal(Client client,string address){
		this->client=client;
		this->address=address;
		isAdopted=false;
		outcome="";
		updateData();
	}
	Removal(REMOVALDATA data){
		interventionNum=data.intervention_num;
		date_time=data.date_time;
		address=data.address;
		isAdopted=data.isAdopted;
		outcome=data.outcome;
		client.setData(data.clientdata);
		//updateData();
		this->data=data;
	}
	void updateData(){
		data.intervention_num=interventionNum;
		strcpy(data.date_time,date_time.c_str());
		strcpy(data.address,address.c_str());
		data.isAdopted=isAdopted;
		strcpy(data.outcome,outcome.c_str());
		data.clientdata=client.getData();
	}
	REMOVALDATA getData(){
		return data;
	}
	void setClient(Client client);
	void setInterventionNum(int num);
	void setData(REMOVALDATA data);
	void setAddress(string address);
	void setOutcome(string outcome);
	void adoption(bool value);
	string getAddress();
	string getOutcome();
	void display();
	~Removal(){}
};

#endif
