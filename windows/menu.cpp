#include <vector>
#include <iostream>
#include <fstream>
#include <cstdlib>
#if (defined (_WIN32) || defined (_WIN64))
	#include <conio.h>//file that contains getch on windows
#elif (defined (LINUX) || defined (__linux__))
	#include <termios.h>
	#include <unistd.h>
#endif
#include <string>
#include <ctime>
#include "Menu.h"

using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::vector;
using std::ifstream;

Menu::Menu(string options[],int size,string instructions){
	this->size=size;
	this->instructions=instructions;
	for(int i=0;i<size;i++){
		this->options.push_back(options[i]);
	}
}
Menu::Menu(int size,string instructions){
	this->size=size;
	this->instructions=instructions;
}
Menu::Menu(string options[],int size){
	this->size=size;
	for(int i=0;i<size;i++){
		this->options.push_back(options[i]);
	}
}
void Menu::setOptions(string options[]){
	for(int i=0;i<size;i++){
		this->options.push_back(options[i]);
	}
}
void Menu::setInstructions(string text){
	instructions=text;
}
void Menu::arrow(int count){
	int i;
	string date_time;
	time_t now= time(0);
	date_time=ctime(&now);
	date_time.pop_back();
	printLogo("logo.txt");
	cout<<"\n\n\t\t\t\t\t\t\t "<<date_time;
	cout<<instructions;
	for(i=0;i<size;i++){
		cout<<"\t\t\t\t\t\t    "<<((i==count)?"<<<-->>>":"        ")<<options[i]<<((i==count)?"<<<-->>>":"        ")<<"\n"<<endl;
	}
}
int Menu::prompt(){
	int key;
	unsigned int count=0;
	arrow(count);
	while(key!=13){
		key=getch();
		if(key==DOWN){
			if(count<(size-1)){
				count++;
			}
		}else if(key==UP && count>0){
				count--;
		}
		arrow(count);
	}
	return count;
}
