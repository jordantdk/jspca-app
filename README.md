#My 2nd Year Object Oriented Programming Project Solution for JSPCA

This project is a solution to the non-profit organization's (Jamaica Society for the Prevention of Cruelty to Animals) problem of storing and maintaining requests made by clients daily.

#Features include (but not limited to):

- Add request
- View request
- Update Request
- View all requests
- Generate report

**NB: These can be applied to either a visit or removal**